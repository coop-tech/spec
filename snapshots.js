const urls = [
  {
    name: 'YAML Spec',
    path: '/examples/using-single-yaml/',
  }
];

module.exports = () => {
  const baseUrl = process.env.TARGET_URL || 'http://localhost:3000';
  return urls.map(({ name, path }) => {
    return {
      name,
      url: `${baseUrl}${path}`,
    };
  });
};
