/**
 * @type {import('redocusaurus').PresetEntry}
 */
const redocusaurus = [
  'redocusaurus',
  {
    debug: Boolean(process.env.DEBUG || process.env.CI),
    specs: [
      {
        id: 'using-single-yaml',
        spec: 'openapi/coopenapi.yaml',
        route: '/openapi/',
      }
    ],
    theme: {
      /**
       * Highlight color for docs
       */
      primaryColor: '#1890ff',
      /**
       * Options to pass to redoc
       * @see https://github.com/redocly/redoc#redoc-options-object
       */
      options: { disableSearch: true },
      /**
       * Options to pass to override RedocThemeObject
       * @see https://github.com/Redocly/redoc#redoc-theme-object
       */
      theme: {},
    },
  },
];

if (process.env.VERCEL_URL) {
  process.env.DEPLOY_PRIME_URL = `https://${process.env.VERCEL_URL}`;
}

/**
 * @type {Partial<import('@docusaurus/types').DocusaurusConfig>}
 */
const config = {
  presets: [
    redocusaurus,
    /** ************ Rest of your Docusaurus Config *********** */
    [
      '@docusaurus/preset-classic',
      {
        debug: Boolean(process.env.DEBUG || process.env.CI),
        theme: { customCss: [require.resolve('./src/custom.css')] },
        docs: {
          routeBasePath: '/docs',
          editUrl: 'https://github.com/rohit-gohri/redocusaurus/edit/main/website/',
        },
      },
    ],
  ],
  title: 'Coop Open API',
  tagline: 'Кооперативное OpenAPI',
  customFields: {
    meta: {
      description: 'Для облегчения интеграции кооперативных информационных систем',
    },
  },
  url: process.env.DEPLOY_PRIME_URL || 'http://localhost:5000', // Your website URL
  baseUrl: process.env.DEPLOY_BASE_URL || '/spec/', // Base URL for your project */
  favicon: 'img/favicon.ico',
  themeConfig: {
    navbar: {
      title: 'CoOpenAPI',
      items: [
        {
          label: 'Документация',
          position: 'left',
          to: '/docs',
        },
        {
          label: 'Спецификация',
          position: 'left',
          to: '/openapi',
        },
        {
          href: 'https://gitlab.com/coop-tech/spec',
          position: 'right',
          className: 'header-github-logo',
          'aria-label': 'Git Repo',
        },
      ],
    },
    footer: {
      // logo: {
      //   alt: 'Redocusaurus Logo',
      //   src: 'img/logoDark.png',
      // },
      style: 'dark',
      links: [
        {
          title: 'Ссылки',
          items: [
            {
              label: 'Группа в Gitlab',
              href: 'https://gitlab.com/coop-tech',
            },
            {
              label: 'Телеграм чат',
              href: 'https://t.me/cooptech',
            },
          ],
        },
      ],
      copyright: `${new Date().getFullYear()}`
    },
  },
};

module.exports = config;
